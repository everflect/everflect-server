# EverflectServer

To start:

  * `docker run --name everflect_pg -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 postgres:alpine`
  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phoenix.server`
