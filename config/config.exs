# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :everflect_server,
  ecto_repos: [EverflectServer.Repo],
  env: Mix.env()

# Configures the endpoint
config :everflect_server, EverflectServer.Web.Endpoint,
  url: [host: "10.0.0.239"],
  secret_key_base: "ISUKG164kLHCJCCm4VMJUHsb1wsC3M5RES9Mrg1hhZS6h2YebVmPB3lmCct5zsFg",
  render_errors: [view: EverflectServer.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: EverflectServer.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure phoenix generators
config :phoenix, :generators, binary_id: true

config :everflect_server, EverflectServer.Mailer,
  adapter: Swoosh.Adapters.AmazonSES,
  region: {:system, "AWS_REGION"},
  access_key: {:system, "AWS_ACCESS_KEY"},
  secret: {:system, "AWS_SECRET_KEY"}

config :everflect_server, EverflectServer.Vault, json_library: Poison

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
