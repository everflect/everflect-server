use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :everflect_server, EverflectServer.Web.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: []

# Watch static and templates for browser reloading.
config :everflect_server, EverflectServer.Web.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{lib/everflect_server/web/views/.*(ex)$},
      ~r{lib/everflect_server/web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :everflect_server, EverflectServer.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "everflect_server_dev",
  hostname: "localhost",
  pool_size: 10

config :sentry,
  dsn:
    "https://9e27e4c9dcb74135bef41f0dfcfdb76e:2914f8d169ec4506a51c970de66ab0fe@sentry.io/207680",
  environment_name: :dev,
  enable_source_code_context: true,
  root_source_code_path: File.cwd!(),
  tags: %{
    env: "development"
  },
  included_environments: [:prod]

config :everflect_server, EverflectServer.GraphQL.Resolver.Purchase,
  android_license_key:
    System.get_env("ANDROID_LICENSE_KEY") || "${ANDROID_LICENSE_KEY}"

config :pigeon, :apns,
  apns_default: %{
    cert: "/Users/coriaria/Projects/everflect-couples/aps.pem",
    key: "/Users/coriaria/Projects/everflect-couples/aps.key",
    mode: :prod
  }

config :pigeon, :fcm,
  fcm_default: %{
    key: System.get_env("FCM_KEY") || "${FCM_KEY}"
  }
