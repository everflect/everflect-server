use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :everflect_server, EverflectServer.Web.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :everflect_server, EverflectServer.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "everflect_server_test",
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox

config :sentry,
  dsn:
    "https://9e27e4c9dcb74135bef41f0dfcfdb76e:2914f8d169ec4506a51c970de66ab0fe@sentry.io/207680",
  environment_name: :dev,
  enable_source_code_context: true,
  root_source_code_path: File.cwd!(),
  tags: %{
    env: "test"
  },
  included_environments: [:test]

config :everflect_server, EverflectServer.GraphQL.Resolver.Purchase,
  apple_url: "https://sandbox.itunes.apple.com/verifyReceipt",
  android_license_key:
    System.get_env("ANDROID_LICENSE_KEY") || "${ANDROID_LICENSE_KEY}"
