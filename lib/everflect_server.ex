defmodule EverflectServer do
  @moduledoc """
  Kickoff Everflect application
  """

  use Application
  alias Ecto.Migrator

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      supervisor(EverflectServer.Repo, []),
      # Start the endpoint when the application starts
      supervisor(EverflectServer.Web.Endpoint, []),
      # Start Absinthe Subscription management
      supervisor(Absinthe.Subscription, [EverflectServer.Web.Endpoint]),
      # Start your own worker by calling:
      #     EverflectServer.Worker.start_link(arg1, arg2, arg3)
      # worker(EverflectServer.Worker, [arg1, arg2, arg3]),
      supervisor(EverflectServer.PushNotificationManager, []),
      worker(EverflectServer.Vault, [])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: EverflectServer.Supervisor]
    sup_ret = Supervisor.start_link(children, opts)

    run_migrations(EverflectServer.Repo)
    sup_ret
  end

  def run_migrations(repo) do
    migrations_path =
      repo.config()
      |> Keyword.fetch!(:otp_app)
      |> Application.app_dir()
      |> Path.join("priv")
      |> Path.join("repo")
      |> Path.join("migrations")

    IO.puts("migrations path: #{inspect(migrations_path)}")
    Migrator.run(repo, migrations_path, :up, all: true)
  end
end
