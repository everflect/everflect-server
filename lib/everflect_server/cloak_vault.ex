defmodule EverflectServer.Vault do
  @moduledoc """
  Cloak vault for encryption/decryption
  """

  use Cloak.Vault, otp_app: :everflect_server

  @impl GenServer
  def init(config) do
    config =
      Keyword.put(config, :ciphers,
        default:
          {Cloak.Ciphers.AES.CTR, tag: "AES.V2", key: decode_env("EVERFLECT_ENCRYPTION_KEY")},
        retired:
          {Cloak.Ciphers.Deprecated.AES.CTR,
           module_tag: "AES", tag: <<1>>, key: decode_env("EVERFLECT_ENCRYPTION_KEY")}
      )

    {:ok, config}
  end

  defp decode_env(var) do
    var
    |> System.get_env()
    |> Base.decode64!()
  end
end

defmodule EverflectServer.Encrypted.Binary do
  @moduledoc """
  Defines an ecto schema type for encrypted binary data
  """
  use Cloak.Fields.Binary, vault: EverflectServer.Vault
end
