defmodule EverflectServer.GraphQL.MW.Authentication do
  @moduledoc """
  Handle authentication for applicable elements from our schema
  """

  @behaviour Absinthe.Middleware
  alias Absinthe.Resolution

  def call(resolution, _config) do
    case resolution.context do
      %{current_user: _} ->
        resolution

      _ ->
        resolution
        |> Resolution.put_result({:error, "unauthenticated"})
    end
  end
end
