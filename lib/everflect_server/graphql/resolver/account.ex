defmodule EverflectServer.GraphQL.Resolver.Account do
  @moduledoc """
  Resolver for Account data
  """

  alias Ecto.UUID
  alias EverflectServer.Mailer
  alias EverflectServer.Repo
  alias EverflectServer.Web.Couple
  alias EverflectServer.Web.LoginToken
  alias EverflectServer.Web.Spouse
  alias EverflectServer.Web.UserSession

  @doc """
  Send a login token to a given email address
  We create the account (if necessary) as well
  """
  def request_login_email(%{email: email}, _info) do
    email = String.downcase(email)

    existing_account =
      Spouse
      |> Repo.get_by(email: email)

    spouse =
      existing_account
      |> is_nil
      |> case do
        true -> generate_spouse(email)
        false -> existing_account
      end

    # Generate login token for spouse and send email
    spouse
    |> generate_spouse_login
    |> case do
      {:ok, login_token} -> login_token
      {:error, _} -> nil
    end
    |> send_login_email(email)

    {:ok, %{id: UUID.generate(), status: "Account exists"}}
  end

  def request_login_email(_args, _info) do
    {:error, "Invalid request"}
  end

  def logout(_args, %{context: %{user_session_id: user_session_id}}) do
    with session <- Repo.get(UserSession, user_session_id),
         {:ok, _deleted_session} <- Repo.delete(session) do
      {:ok, %{id: UUID.generate(), status: "Logged out"}}
    else
      _ -> {:error, "Logout failed"}
    end
  end

  def logout(_args, _info) do
    {:error, "Authentication required"}
  end

  @doc """

  """
  def exchange_token_for_session(%{token: token}, _info) do
    # 1. Look up spouse by token, ensuring validity in the last 5 minutes
    with time_frame <- Timex.shift(Timex.now(), minutes: -5),
         [spouse_id_raw, login_token_id_raw] <-
           exchange_token_query(token, time_frame),
         {:ok, spouse_id} <- UUID.load(spouse_id_raw),
         {:ok, login_token_id} <- UUID.load(login_token_id_raw),
         # 2. Generate session, insert and associate with user
         {:ok, %UserSession{id: session_id, token: sesion_token}} <-
           insert_session_token(spouse_id, token, login_token_id) do
      # 3. Send session back to user
      {:ok, %{id: session_id, session: sesion_token}}
    else
      _ -> {:error, "Invalid token"}
    end
  end

  def exchange_token_for_session(_args, _info) do
    {:error, "Invalid request"}
  end

  defp exchange_token_query(login_token, time_frame) do
    import Ecto.Query

    query =
      from(
        l in "login_tokens",
        where: l.token == ^login_token and l.inserted_at > ^time_frame,
        select: [l.spouse_id, l.id]
      )

    Repo.one(query)
  end

  defp insert_session_token(spouse_id, login_token, login_token_id) do
    Repo.transaction(fn ->
      session_token = Randomizer.randomizer(50, :all)

      user_session =
        %UserSession{spouse_id: spouse_id, token: session_token}
        |> UserSession.changeset()
        |> Repo.insert!()

      %LoginToken{id: login_token_id, spouse_id: spouse_id, token: login_token}
      |> LoginToken.changeset()
      |> Repo.delete!()

      user_session
    end)
  end

  defp generate_spouse(email) do
    transaction_result =
      Repo.transaction(fn ->
        {:ok, inserted_spouse} =
          %Spouse{email: email}
          |> Spouse.changeset()
          |> Repo.insert()

        {:ok, inserted_couple} =
          %Couple{
            spouseA: inserted_spouse,
            join_token: Randomizer.randomizer(5)
          }
          |> Couple.changeset()
          |> Repo.insert()

        %{spouse: inserted_spouse, couple: inserted_couple}
      end)

    case transaction_result do
      {:ok, %{spouse: inserted_spouse, couple: _inserted_couple}} ->
        inserted_spouse

      {:error, :rollback} ->
        nil
    end
  end

  defp generate_spouse_login(spouse) do
    %LoginToken{spouse_id: spouse.id, token: Randomizer.randomizer(30, :all)}
    |> LoginToken.changeset()
    |> Repo.insert()
  end

  defp send_login_email(login_token, email) do
    import Swoosh.Email

    token_url = url_for_token(login_token)

    new()
    |> to(email)
    |> from({"Everflect Login", "login-noreply@everflect.com"})
    |> subject("Login to Everflect")
    |> html_body("""
        <p><a href="#{token_url}">Click here</a> to finish logging into Everflect</p>
        <p>Or paste the following into your browser: #{token_url}</p>
        <p>Please note that this link will expire in five minutes</p>
        <p>If you did not request a login link, you can safely delete this message.</p>
    """)
    |> text_body("""
    Use this link to finish logging into Everflect: #{token_url}
    This link will expire in 5 minutes.
    If you did not request a login link, you can safely delete this message.
    """)
    |> Mailer.deliver()
    |> case do
      {:error, error} -> error
      {:ok, response} -> response
    end
  end

  defp url_for_token(login_token) do
    environment = Application.get_env(:everflect_server, :env)

    scheme =
      case environment do
        :prod -> "https://api.everflect.com"
        _ -> "http://192.168.1.16:4000"
      end

    "#{scheme}/loginRedirect/#{login_token.id}"
  end
end
