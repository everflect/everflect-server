defmodule EverflectServer.GraphQL.Resolver.Couple do
  @moduledoc """
  Resolver for Couple data
  """

  alias EverflectServer.Repo
  alias EverflectServer.Web.Couple

  # Resolvers take 2 or 3 arguments
  # /2 (field args, Absinthe.Resolution struct)
  # /3 (parent, field arguments, Absinthe.Resolution struct)
  # For Absinthe.Resolution, see:
  #    https://hexdocs.pm/absinthe/Absinthe.Resolution.html#t:t/0-contents

  def couple(_args, %{context: %{couple: couple}}) do
    {:ok, %{id: couple.id, joined_course: couple.joined_course}}
  end

  def couple(_args, _info) do
    {:error, "Authentication required"}
  end

  def joinToken(_args, %{context: %{couple: %{join_token: token}}}) do
    {:ok, %{token: token}}
  end

  def joinToken(_args, _info) do
    {:error, "Authentication required"}
  end

  def use_join_token(%{token: token}, %{
        context: %{couple: %{join_token: existing_token}}
      })
      when existing_token === token do
    {:error, "You can't use your own join token."}
  end

  def use_join_token(%{token: token}, %{context: %{spouse: spouse}}) do
    # Match the join token with a couple missing a spouseB
    token = String.upcase(token)

    spouse
    |> Couple.join_spouse_with_token(token)
    |> case do
      {:ok, results} -> {:ok, results}
      _ -> {:error, "Undefined"}
    end
  end

  def use_join_token(_args, _info) do
    {:error, "Authentication required"}
  end

  def join_course(%{join_course: join_course}, %{context: %{couple: couple}}) do
    couple
    |> Couple.changeset(%{joined_course: join_course})
    |> Repo.update()
  end
end
