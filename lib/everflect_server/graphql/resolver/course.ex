defmodule EverflectServer.GraphQL.Resolver.Course do
  @moduledoc """
  Resolver for Course data
  """

  alias EverflectServer.Repo
  alias EverflectServer.Web.Course

  def all(_args, %{context: %{spouse: _spouse}}) do
    result =
      Course
      |> Repo.all()

    {:ok, result}
  end

  def all(_args, _info) do
    {:error, "Authentication required"}
  end
end
