defmodule EverflectServer.GraphQL.Resolver.Inventory do
  @moduledoc """
  Resolver for Inventory data
  """

  alias EverflectServer.PushNotificationManager
  alias EverflectServer.Repo
  alias EverflectServer.Web.Couple
  alias EverflectServer.Web.Inventory
  alias EverflectServer.Web.Spouse
  alias Timex.Duration

  import Ecto.Query

  require Logger

  # Resolvers take 2 or 3 arguments
  # /2 (field args, Absinthe.Resolution struct)
  # /3 (parent, field arguments, Absinthe.Resolution struct)
  # For Absinthe.Resolution, see:
  #    https://hexdocs.pm/absinthe/Absinthe.Resolution.html#t:t/0-contents

  # def all(_args, %{context: %{spouse: spouse}}) do
  #     query = from i in Inventory,
  #         join: c in Couple, on: i.couple_id == c.id,
  #         join: s in Spouse, on: s.id == c.spouse_a_id
  #             or s.id == c.spouse_b_id,
  #         where: s.device_id == ^spouse.device_id

  #     inventories = Repo.all(query)
  #     {:ok, inventories}
  # end

  # def all(_args, _info) do
  #     {:error, "Authentication required"}
  # end

  def paginated(%{limit: limit, offset: offset}, %{context: %{spouse: spouse}}) do
    full_limit =
      spouse
      |> Spouse.verify_purchase("all-inventories")
      |> case do
        true -> limit
        false -> 2
      end

    query =
      from(
        i in Inventory,
        join: c in Couple,
        on: i.couple_id == c.id,
        join: s in Spouse,
        on: s.id == c.spouse_a_id or s.id == c.spouse_b_id,
        where: s.id == ^spouse.id and i.inventory_status == "completed",
        limit: ^full_limit,
        offset: ^offset,
        order_by: [desc: :inserted_at]
      )

    inventories =
      query
      |> Repo.all()
      |> Enum.map(fn i ->
        {:ok, inventory} = inventory_to_gql_response(i, spouse)
        inventory
      end)

    {:ok, inventories}
  end

  def paginated(_args, _info) do
    {:error, "Authentication required"}
  end

  def current_inventory(_args, %{context: %{spouse: spouse}}) do
    spouse
    |> Couple.couple_from_spouse()
    |> current_inventory_from_couple
    |> inventory_to_gql_response(spouse)
  end

  def current_inventory(_args, _info) do
    {:error, "Authentication required"}
  end

  def previous_inventory(_args, %{context: %{spouse: spouse}}) do
    spouse
    |> Couple.couple_from_spouse()
    |> previous_inventory_from_couple
    |> inventory_to_gql_response(spouse)
  end

  def previous_inventory(_args, _info) do
    {:error, "Authentication required"}
  end

  def inventory(%{inventory_id: inventory_id}, %{
        context: %{couple: %{id: couple_id}, spouse: spouse}
      }) do
    query =
      from(
        i in Inventory,
        where: i.couple_id == ^couple_id and i.id == ^inventory_id
      )

    query
    |> Repo.one()
    |> inventory_to_gql_response(spouse)
  end

  def inventory(_args, _info) do
    {:error, "Authentication required"}
  end

  def metadata(_args, %{context: %{couple: %{id: couple_id}}}) do
    query =
      from(
        i in Inventory,
        select: {count(i.id)},
        where: i.couple_id == ^couple_id and not is_nil(i.next_inventory)
      )

    {count} = Repo.one(query)

    {:ok, %{count: count}}
  end

  def metadata(_args, _info) do
    {:error, "Authentication required"}
  end

  # Mutations

  def start_inventory(_args, %{context: %{spouse: spouse}}) do
    # 1.0 Determine if we already have an inventory started at this point
    # 2.1 If we don't, create a new inventory and shift the status
    # 2.2 If we do, shift the status to "in progress"

    couple = spouse |> Couple.couple_from_spouse()

    couple
    |> current_inventory_from_couple
    |> case do
      nil -> create_new_inventory_pending_spouse(couple, spouse)
      inventory -> update_inventory_with_spouse_join(inventory, spouse)
    end
    |> case do
      {:ok, inventory} -> inventory_to_gql_response(inventory, spouse)
      {:error, changeset} -> ecto_errors_to_gql(changeset)
    end
  end

  def start_inventory(_args, _info) do
    {:error, "Authentication required"}
  end

  def advance_spouse_step(_args, %{context: %{spouse: spouse}}) do
    couple = Couple.couple_from_spouse(spouse)

    couple
    |> current_inventory_from_couple
    |> case do
      nil -> {:error, %{message: "No current inventory"}}
      inventory -> advance_inventory_spouse_step(inventory, spouse)
    end
    |> case do
      {:ok, _inventory} -> {:ok, %{success: true}}
      {:error, %{message: message}} -> {:error, %{message: message}}
      {:error, changeset} -> ecto_errors_to_gql(changeset)
    end
  end

  def advance_spouse_step(_args, _info) do
    {:error, "Authentication required"}
  end

  def rollback_spouse_step(_args, %{context: %{spouse: spouse}}) do
    couple = Couple.couple_from_spouse(spouse)

    couple
    |> current_inventory_from_couple
    |> case do
      nil -> {:error, %{message: "No current inventory"}}
      inventory -> rollback_inventory_spouse_step(inventory, spouse)
    end
    |> case do
      {:ok, _inventory} -> {:ok, %{success: true}}
      {:error, %{message: message}} -> {:error, %{message: message}}
      {:error, changeset} -> ecto_errors_to_gql(changeset)
    end
  end

  def rollback_spouse_step(_args, _info) do
    {:error, "Authentication required"}
  end

  def set_spouse_goals(%{goal: goal, help: help}, %{context: %{spouse: spouse}}) do
    spouse
    |> Couple.couple_from_spouse()
    |> current_inventory_from_couple
    |> case do
      nil -> {:error, %{message: "No current inventory"}}
      inventory -> apply_goal_and_help(inventory, spouse, goal, help)
    end
    |> case do
      {:ok, _inventory} -> {:ok, %{success: true}}
      {:error, %{message: message}} -> {:error, %{message: message}}
      {:error, changeset} -> ecto_errors_to_gql(changeset)
    end
  end

  def set_spouse_goals(_args, _info) do
    {:error, "Authentication required"}
  end

  def set_spouse_praise(
        %{
          praise1: praise1,
          praise2: praise2,
          praise3: praise3
        },
        %{context: %{spouse: spouse}}
      ) do
    spouse
    |> Couple.couple_from_spouse()
    |> current_inventory_from_couple
    |> case do
      nil -> {:error, %{message: "No current inventory"}}
      inventory -> apply_praise(inventory, spouse, praise1, praise2, praise3)
    end
    |> case do
      {:ok, _inventory} -> {:ok, %{success: true}}
      {:error, %{message: message}} -> {:error, %{message: message}}
      {:error, changeset} -> ecto_errors_to_gql(changeset)
    end
  end

  def set_spouse_praise(_args, _info) do
    {:error, "Authentication required"}
  end

  def set_spouse_improvement(%{improvement: improvement}, %{
        context: %{spouse: spouse}
      }) do
    spouse
    |> Couple.couple_from_spouse()
    |> current_inventory_from_couple
    |> case do
      nil -> {:error, %{message: "No current inventory"}}
      inventory -> apply_improvement(inventory, spouse, improvement)
    end
    |> case do
      {:ok, _inventory} -> {:ok, %{success: true}}
      {:error, %{message: message}} -> {:error, %{message: message}}
      {:error, changeset} -> ecto_errors_to_gql(changeset)
    end
  end

  def set_spouse_improvement(_args, _info) do
    {:error, "Authentication required"}
  end

  def schedule_everflect(%{inventory_datetime: inventory_datetime}, %{
        context: %{spouse: spouse}
      }) do
    spouse
    |> Couple.couple_from_spouse()
    |> current_inventory_from_couple
    |> case do
      nil -> {:error, %{message: "No current inventory"}}
      inventory -> apply_schedule(inventory, inventory_datetime)
    end
    |> case do
      {:ok, _inventory} -> {:ok, %{success: true}}
      {:error, %{message: message}} -> {:error, %{message: message}}
      {:error, changeset} -> ecto_errors_to_gql(changeset)
    end
  end

  def schedule_everflect(_args, _info) do
    {:error, "Authentication required"}
  end

  # Helpers

  defp apply_schedule(inventory, inventory_datetime) do
    # Create and apply a changeset for the spouse in question
    changeset =
      %{
        spouse_a_step: inventory.spouse_a_step + 1,
        spouse_b_step: inventory.spouse_b_step + 1,
        inventory_status: "completed"
      }
      |> Map.put(:next_inventory, inventory_datetime)

    {:ok, updated_inventory} =
      inventory
      |> Inventory.changeset(changeset)
      |> Repo.update()

    updated_inventory
    |> schedule_notifications

    {:ok, updated_inventory}
  end

  defp apply_improvement(inventory, spouse, improvement) do
    # Figure out which spouse they are
    # (we set their praise fields)
    which_spouse = {
      inventory.couple.spouse_a_id == spouse.id,
      inventory.couple.spouse_b_id == spouse.id
    }

    improvement_spouse_field =
      which_spouse
      |> case do
        {true, false} -> :spouse_a_improvement
        {false, true} -> :spouse_b_improvement
      end

    # Create and apply a changeset for the spouse in question
    changeset =
      %{
        spouse_a_step: inventory.spouse_a_step + 1,
        spouse_b_step: inventory.spouse_b_step + 1
      }
      |> Map.put(improvement_spouse_field, improvement)

    inventory
    |> Inventory.changeset(changeset)
    |> Repo.update()
  end

  defp apply_praise(inventory, spouse, praise1, praise2, praise3) do
    # Figure out which spouse they are
    # (we set their praise fields)
    which_spouse = {
      inventory.couple.spouse_a_id == spouse.id,
      inventory.couple.spouse_b_id == spouse.id
    }

    praised_spouse_field =
      which_spouse
      |> case do
        {true, false} -> "spouse_b_praise"
        {false, true} -> "spouse_a_praise"
      end

    # Create and apply a changeset for the spouse in question
    changeset =
      %{
        spouse_a_step: inventory.spouse_a_step + 1,
        spouse_b_step: inventory.spouse_b_step + 1
      }
      |> Map.put(String.to_atom(praised_spouse_field <> "1"), praise1)
      |> Map.put(String.to_atom(praised_spouse_field <> "2"), praise2)
      |> Map.put(String.to_atom(praised_spouse_field <> "3"), praise3)

    inventory
    |> Inventory.changeset(changeset)
    |> Repo.update()
  end

  defp apply_goal_and_help(inventory, spouse, goal, help) do
    # Figure out which spouse we are and are not
    # (we set their goal field and our own help field)
    which_spouse = {
      inventory.couple.spouse_a_id == spouse.id,
      inventory.couple.spouse_b_id == spouse.id
    }

    goal_spouse_field =
      which_spouse
      |> case do
        {true, false} -> :spouse_b_goal
        {false, true} -> :spouse_a_goal
      end

    help_spouse_field =
      which_spouse
      |> case do
        {true, false} -> :spouse_a_goal_support
        {false, true} -> :spouse_b_goal_support
      end

    # Create and apply a changeset for the spouse in question
    changeset =
      %{
        spouse_a_step: inventory.spouse_a_step + 1,
        spouse_b_step: inventory.spouse_b_step + 1
      }
      |> Map.put(goal_spouse_field, goal)
      |> Map.put(help_spouse_field, help)

    inventory
    |> Inventory.changeset(changeset)
    |> Repo.update()
  end

  defp current_inventory_from_couple(couple) do
    query =
      from(
        i in Inventory,
        where: i.couple_id == ^couple.id and i.inventory_status != "completed",
        order_by: [desc: i.inserted_at],
        limit: 1
      )

    query
    |> Repo.one()
    |> Repo.preload([:couple, :first_spouse])
  end

  defp previous_inventory_from_couple(couple) do
    query =
      from(
        i in Inventory,
        where: i.couple_id == ^couple.id and i.inventory_status == "completed",
        order_by: [desc: i.inserted_at],
        limit: 1
      )

    query
    |> Repo.one()
    |> Repo.preload([:couple, :first_spouse])
  end

  # Using this inventory, shift it to in progress, and return it
  defp update_inventory_with_spouse_join(inventory, spouse) do
    status = {
      inventory.inventory_status,
      inventory.couple.spouse_a_id == spouse.id
    }

    {new_status, step_amount} =
      case status do
        {"waiting_for_a", true} -> {"in_progress", 1}
        {"waiting_for_a", false} -> {"waiting_for_a", 0}
        {"waiting_for_b", true} -> {"waiting_for_b", 0}
        {"waiting_for_b", false} -> {"in_progress", 1}
        {"in_progress", _} -> {"in_progress", 0}
      end

    new_changeset =
      inventory
      |> shift_inventory_spouse_step(spouse, step_amount)
      |> Map.merge(%{inventory_status: new_status})

    inventory
    |> Inventory.changeset(new_changeset)
    |> Repo.update()
  end

  defp shift_inventory_spouse_step(inventory, spouse, amount \\ 1) do
    case inventory.couple.spouse_a_id == spouse.id do
      true -> %{spouse_a_step: inventory.spouse_a_step + amount}
      false -> %{spouse_b_step: inventory.spouse_b_step + amount}
    end
  end

  # Create inventory that's marked as waiting on the other spouse
  defp create_new_inventory_pending_spouse(couple, spouse) do
    new_status =
      cond do
        spouse.id == couple.spouse_a_id -> "waiting_for_b"
        spouse.id == couple.spouse_b_id -> "waiting_for_a"
        true -> "INVALID_INVENTORY_STATUS"
      end

    step_map = step_map_from_spouse_couple(spouse, couple)

    %Inventory{}
    |> Inventory.changeset(%{
      couple_id: couple.id,
      first_spouse_id: couple.spouse_a_id,
      inventory_status: new_status
    })
    |> Inventory.changeset(step_map)
    |> Repo.insert()
  end

  defp step_map_from_spouse_couple(spouse, couple) do
    # spouse_a_step and spouse_b_step start at 1 if we have a
    # previous inventory, 2 if we need to jump right in

    previous_inventory = previous_inventory_from_couple(couple)

    cond do
      couple.spouse_a_id == spouse.id && previous_inventory == nil ->
        %{spouse_a_step: 2, spouse_b_step: 1}

      couple.spouse_b_id == spouse.id && previous_inventory == nil ->
        %{spouse_b_step: 2, spouse_a_step: 1}

      couple.spouse_a_id == spouse.id && previous_inventory != nil ->
        %{spouse_a_step: 1, spouse_b_step: 0}

      couple.spouse_b_id == spouse.id && previous_inventory != nil ->
        %{spouse_b_step: 1, sposue_a_step: 0}
    end
  end

  # Take the current spouse and advance the spouse's step for the
  # given inventory
  defp advance_inventory_spouse_step(inventory, spouse) do
    changeset_map =
      inventory
      |> shift_inventory_spouse_step(spouse)

    inventory
    |> Inventory.changeset(changeset_map)
    |> Repo.update()
  end

  defp rollback_inventory_spouse_step(inventory, spouse) do
    changeset_map =
      inventory
      |> shift_inventory_spouse_step(spouse, -1)

    inventory
    |> Inventory.changeset(changeset_map)
    |> Repo.update()
  end

  # Format an inventory object for a GQL response
  defp inventory_to_gql_response(inventory, spouse) do
    inventory
    |> Repo.preload([:couple, :first_spouse])
    |> format_inventory_for_me(spouse)
    |> case do
      {:error, %{errors: errors}} -> {:error, ecto_errors_to_gql(errors)}
      inventory -> {:ok, inventory}
    end
  end

  # Convert a failed changeset to a GraphQL Error Response
  defp ecto_errors_to_gql(errors) do
    errors
    |> Enum.map(fn {field_name, {message, failures}} ->
      %{
        message: Atom.to_string(field_name) <> ": " <> message,
        field_name: field_name,
        details: failures
      }
    end)
  end

  # Convert an inventory to a given spouse's perspective
  defp format_inventory_for_me(nil, _spouse) do
    nil
  end

  defp format_inventory_for_me(inventory, spouse) do
    my_letter =
      case inventory.couple.spouse_a_id == spouse.id do
        true -> "a"
        false -> "b"
      end

    spouse_letter =
      my_letter
      |> case do
        "a" -> "b"
        "b" -> "a"
      end

    # Convert the Inventory to an enumerable entity
    # Convert the map keys to the perspective of the user
    inventory
    |> Repo.preload([:couple, :first_spouse])
    |> Map.from_struct()
    |> Enum.reduce(%{}, fn {key, value}, acc ->
      new_key =
        key
        |> Atom.to_string()
        |> String.replace("spouse_" <> my_letter, "my")
        |> String.replace("spouse_" <> spouse_letter, "spouse")
        |> String.to_atom()

      Map.put(acc, new_key, value)
    end)
    |> Map.put(:inventory_status, inv_status_for_spouse(inventory, my_letter))
    |> Map.put(:first_spouse_me, inventory.first_spouse_id == spouse.id)
  end

  defp inv_status_for_spouse(inventory, my_spouse_letter) do
    inventory.inventory_status
    |> case do
      "waiting_for_" <> spouse_letter when spouse_letter == my_spouse_letter ->
        "waiting_for_self"

      "waiting_for_" <> spouse_letter when spouse_letter != my_spouse_letter ->
        "waiting_for_spouse"

      default ->
        default
    end
  end

  defp schedule_notifications(inventory) do
    schedule_one_week_notification(inventory)
    schedule_halfway_notification(inventory)
  end

  defp schedule_one_week_notification(inventory) do
    datetime = inventory.next_inventory

    Logger.info(
      "Scheduling notification for inventory #{inventory.id} for #{datetime}"
    )

    PushNotificationManager.schedule_inventory_notification(inventory, datetime)
  end

  defp schedule_halfway_notification(inventory) do
    current_moment = Timex.now()

    next_inventory_in_seconds =
      inventory.next_inventory
      |> Timex.diff(current_moment)
      |> Duration.from_microseconds()
      |> Duration.to_seconds()

    halfway_moment_in_seconds = next_inventory_in_seconds / 2
    halfway_moment = Duration.from_seconds(halfway_moment_in_seconds)

    future_datetime = Timex.add(current_moment, halfway_moment)

    Logger.info(
      "Scheduling halfway notification for inventory #{inventory.id} for #{
        future_datetime
      }"
    )

    PushNotificationManager.schedule_inventory_notification(
      inventory,
      future_datetime
    )
  end
end
