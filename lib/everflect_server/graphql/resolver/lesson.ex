defmodule EverflectServer.GraphQL.Resolver.Lesson do
  @moduledoc """
  Resolver for Lesson data
  """

  alias EverflectServer.Repo
  alias EverflectServer.Web.Couple
  alias EverflectServer.Web.Lesson
  alias EverflectServer.Web.Purchase
  alias EverflectServer.Web.Spouse
  alias EverflectServer.Web.SpousePurchase

  import Ecto.Query

  def course_lessons(%{course_id: course_id}, %{context: %{spouse: spouse}}) do
    # Get all lessons
    all_lesson_query =
      from(
        l in Lesson,
        where: l.course_id == ^course_id,
        order_by: l.inserted_at
      )

    all_lessons = all_lesson_query |> Repo.all()

    # Get all available lesson IDs
    available_lesson_query =
      from(
        l in Lesson,
        join: p in Purchase,
        on: p.id == l.required_purchase_id,
        join: sp in SpousePurchase,
        on: p.id == sp.purchase_id,
        join: s1 in Spouse,
        on: s1.id == sp.spouse_id,
        join: c in Couple,
        on: c.spouse_a_id == s1.id or c.spouse_b_id == s1.id,
        join: s2 in Spouse,
        on: c.spouse_a_id == s2.id or c.spouse_b_id == s2.id,
        where:
          l.course_id == ^course_id and
            (s1.id == ^spouse.id or s2.id == ^spouse.id),
        select: {l.id},
        distinct: true
      )

    available_lesson_ids =
      available_lesson_query
      |> Repo.all()
      |> Enum.map(fn {lesson_id} -> lesson_id end)

    # Replace lesson content with available content
    result = strip_unpurchased_lessons(all_lessons, available_lesson_ids)

    {:ok, result}
  end

  def course_lessons(_args, _info) do
    {:error, "Authentication required"}
  end

  # Helpers
  defp strip_unpurchased_lessons(lessons, available_lesson_ids) do
    lessons
    |> Enum.map(fn lesson ->
      if !is_nil(lesson.required_purchase_id) and
           !Enum.member?(available_lesson_ids, lesson.id) do
        Map.put(lesson, :lesson_available, false)
      else
        Map.put(lesson, :lesson_available, true)
      end
    end)
    |> Enum.map(fn lesson ->
      if lesson.lesson_available do
        lesson
      else
        Map.put(lesson, :content, "<p>This content has not been purchased</p>")
      end
    end)
  end
end
