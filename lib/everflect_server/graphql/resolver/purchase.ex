defmodule EverflectServer.GraphQL.Resolver.Purchase do
  @moduledoc """
  Resolver for Purchase data
  """

  alias EverflectServer.Repo
  alias EverflectServer.Web.Purchase
  alias EverflectServer.Web.SpousePurchase

  import Ecto.Query

  @android_pub_key Application.get_env(:everflect_server, __MODULE__)[
                     :android_license_key
                   ]
                   |> ExPublicKey.loads()

  def all(_args, %{context: %{couple: couple, spouse: spouse}}) do
    query =
      from(
        p in Purchase,
        left_join: sp in SpousePurchase,
        on: sp.purchase_id == p.id,
        where:
          sp.spouse_id == ^couple.spouse_a_id or
            sp.spouse_id == ^couple.spouse_b_id or is_nil(sp.spouse_id),
        select: %{
          id: p.id,
          purchase_key: p.purchase_key,
          description: p.description,
          purchasing_spouse_id: sp.spouse_id
        },
        distinct: true
      )

    result =
      query
      |> Repo.all()
      |> format_purchases(spouse, couple)

    {:ok, result}
  end

  def all(_args, _info) do
    {:error, "Authentication required"}
  end

  def one(%{purchase_key: purchase_key}, %{
        context: %{couple: couple, spouse: spouse}
      }) do
    valid_spouse_ids =
      [couple.spouse_a_id, couple.spouse_b_id]
      |> Enum.filter(fn el -> !is_nil(el) end)

    query =
      from(
        p in Purchase,
        left_join: sp in SpousePurchase,
        on: sp.purchase_id == p.id,
        where: sp.spouse_id in ^valid_spouse_ids,
        where: p.purchase_key == ^purchase_key,
        select: %{
          id: p.id,
          purchase_key: p.purchase_key,
          description: p.description,
          purchasing_spouse_id: sp.spouse_id
        },
        distinct: true
      )

    result =
      query
      |> Repo.one()
      |> format_purchase(spouse, couple)

    {:ok, result}
  end

  def one(_args, _info) do
    {:error, "Authentication required"}
  end

  def confirm_purchase(%{receipt: receipt, platform: "ios"}, %{
        context: %{spouse: spouse}
      }) do
    # Validate receipt contents with Apple
    with {:ok, payload} <- Poison.encode(%{"receipt-data": receipt}),
         {:ok, in_app_purchases} <- verify_itunes_with_sandbox_retry(payload) do
      # Based on validation, setup appropriate purchase
      # Convert purchase key into purchase id
      in_app_purchases
      |> Enum.map(fn %{"product_id" => product_id} ->
        Repo.get_by(Purchase, purchase_key: product_id)
      end)
      |> Enum.map(fn purchase ->
        receipt_spouse_purchase(spouse.id, purchase.id)
      end)
      |> Enum.reject(&is_nil/1)
      |> Enum.each(fn sp_changeset -> Repo.insert_or_update(sp_changeset) end)

      {:ok, %{id: UUID.uuid4(), status: "validated"}}
    else
      status -> {:ok, %{id: UUID.uuid4(), status: status}}
    end
  end

  def confirm_purchase(
        %{receipt: receipt, platform: "android", signature: signature},
        %{
          context: %{spouse: spouse}
        }
      ) do
    # Validate receipt locally using certificate
    with {:ok, public_key} <- @android_pub_key,
         {:ok, decoded_sig} <- Base.decode64(signature),
         {:ok, true} <-
           ExPublicKey.verify(receipt, :sha, decoded_sig, public_key),
         {:ok,
          %{"packageName" => "com.everflect.couples", "productId" => product_id}} <-
           Poison.decode(receipt),
         purchase <- Repo.get_by(Purchase, purchase_key: product_id),
         sp_changeset <- receipt_spouse_purchase(spouse.id, purchase.id) do
      # Based on validation, setup appropriate purchase
      sp_changeset
      |> is_nil
      |> case do
        false -> Repo.insert_or_update(sp_changeset)
        true -> {:ok, nil}
      end
      |> case do
        {:ok, _response} -> {:ok, %{id: UUID.uuid4(), status: "validated"}}
        _ -> {:error, "Error validating purchase"}
      end
    else
      _ -> {:ok, %{id: UUID.uuid4(), status: "invalid purchase"}}
    end
  end

  def confirm_purchase(_args, _info) do
    {:error, "Authentication required"}
  end

  defp verify_itunes_with_sandbox_retry(payload) do
    verify_itunes_with_sandbox_retry(payload, :prod)
  end

  defp verify_itunes_with_sandbox_retry(payload, :prod) do
    with apple_url <- "https://buy.itunes.apple.com/verifyReceipt",
         {:ok, %{body: body, status_code: 200}} <-
           HTTPoison.post(apple_url, payload, [{"Accept", "application/json"}]),
         {:ok,
          %{"receipt" => %{"in_app" => in_app_purchases}, "status" => status}} <-
           Poison.decode(body),
         0 <- status do
      {:ok, in_app_purchases}
    else
      # Sample of how to pattern match on various stati
      # {:ok, %{"status" => 21007}} -> {:error, :incorrect_environment}
      _ ->
        verify_itunes_with_sandbox_retry(payload, :dev)
    end
  end

  defp verify_itunes_with_sandbox_retry(payload, :dev) do
    with apple_url <- "https://sandbox.itunes.apple.com/verifyReceipt",
         {:ok, %{body: body, status_code: 200}} <-
           HTTPoison.post(apple_url, payload, [{"Accept", "application/json"}]),
         {:ok,
          %{"receipt" => %{"in_app" => in_app_purchases}, "status" => status}} <-
           Poison.decode(body),
         0 <- status do
      {:ok, in_app_purchases}
    else
      status -> {:error, status}
    end
  end

  defp receipt_spouse_purchase(spouse_id, purchase_id) do
    with spouse_purchase <-
           Repo.get_by(
             SpousePurchase,
             spouse_id: spouse_id,
             purchase_id: purchase_id
           ),
         false <- is_nil(spouse_purchase) do
      nil
    else
      _ ->
        SpousePurchase.changeset(%SpousePurchase{}, %{
          spouse_id: spouse_id,
          purchase_id: purchase_id
        })
    end
  end

  defp format_purchase(nil, _, _) do
    nil
  end

  defp format_purchase(purchase, spouse, couple) do
    purchase
    |> Map.put(:my_purchase, purchase.purchasing_spouse_id == spouse.id)
    |> Map.put(
      :couple_purchase,
      purchase.purchasing_spouse_id == couple.spouse_a_id or
        purchase.purchasing_spouse_id == couple.spouse_b_id
    )
  end

  defp format_purchases(purchases, spouse, couple) do
    purchases
    |> Enum.map(fn purchase ->
      format_purchase(purchase, spouse, couple)
    end)
  end
end
