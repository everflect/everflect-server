defmodule EverflectServer.GraphQL.Resolver.PushNotification do
  @moduledoc """
  Resolver for Push notification handling
  """

  alias EverflectServer.Repo
  alias EverflectServer.Web.PushToken

  @doc """
  Register a push token to a spouse
  """
  def register_push_token(%{push_token: push_token, platform: platform}, %{
        context: %{spouse: spouse}
      }) do
    with {:ok, normalized_platform} <- normalize_platform(platform),
         {:ok, _token} <-
           create_token(push_token, spouse.id, normalized_platform) do
      {:ok, spouse}
    else
      _ -> {:ok, spouse}
    end
  end

  def register_push_token(%{push_token: push_token}, info) do
    register_push_token(%{push_token: push_token, platform: "expo"}, info)
  end

  def register_push_token(_args, _info) do
    {:error, "Authentication required"}
  end

  defp normalize_platform("android"), do: {:ok, "android"}
  defp normalize_platform("ios"), do: {:ok, "ios"}
  defp normalize_platform("expo"), do: {:ok, "expo"}
  defp normalize_platform(_), do: {:error, "unknown platform"}

  defp create_token(push_token, spouse_id, platform) do
    # We don't check if the insert works, since
    # we expect there to be unique constraint
    # violations. This is a lazy upsert.
    %PushToken{}
    |> PushToken.changeset(%{
      spouse_id: spouse_id,
      token: push_token,
      token_type: platform
    })
    |> Repo.insert()
  end
end
