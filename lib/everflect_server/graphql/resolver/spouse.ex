defmodule EverflectServer.GraphQL.Resolver.Spouse do
  @moduledoc """
  Resolver for Spouse data
  """

  alias EverflectServer.Repo
  alias EverflectServer.Web.Couple
  alias EverflectServer.Web.Spouse

  alias Ecto.Changeset

  import Ecto.Query

  # Resolvers take 2 or 3 arguments
  # /2 (field args, Absinthe.Resolution struct)
  # /3 (parent, field arguments, Absinthe.Resolution struct)
  # For Absinthe.Resolution, see:
  #    https://hexdocs.pm/absinthe/Absinthe.Resolution.html#t:t/0-contents

  def mySelf(_args, %{context: %{spouse: spouse}}) do
    {:ok, spouse}
  end

  def mySelf(_args, _info) do
    {:error, "Authentication required"}
  end

  def joinToken(_args, %{context: %{couple: couple}}) do
    {:ok, couple.join_token}
  end

  def joinToken(_args, _info) do
    {:error, "Authentication required"}
  end

  def mySpouse(_args, %{context: %{spouse: spouse}}) do
    query =
      from(
        c in Couple,
        join: s1 in Spouse,
        on:
          (c.spouse_a_id == s1.id or c.spouse_b_id == s1.id) and
            s1.id == ^spouse.id,
        join: s2 in Spouse,
        on:
          (c.spouse_a_id == s2.id or c.spouse_b_id == s2.id) and
            s2.id != ^spouse.id,
        select: {s2.name}
      )

    query
    |> Repo.one()
    |> case do
      {spouse_name} -> {:ok, %{name: spouse_name}}
      nil -> {:ok, nil}
    end
  end

  def mySpouse(_args, _info) do
    {:error, "Authentication required"}
  end

  # Mutations

  def set_my_name(%{name: name}, %{context: %{spouse: spouse}}) do
    spouse
    |> Changeset.change(%{name: name})
    |> Repo.update()
  end

  def set_my_name(_args, _info) do
    {:error, "Authentication required"}
  end

  def setup_spouse(%{name: name, email: email}, %{context: %{spouse: spouse}}) do
    spouse
    |> Changeset.change(%{name: name, email: email})
    |> Repo.update()
  end

  def setup_spouse(_args, _info) do
    {:error, "Authentication required"}
  end

  def join_email_course(%{email: email}, %{context: %{spouse: spouse}}) do
    spouse
    |> Changeset.change(%{email: email, joined_course: true})
    |> Repo.update()
  end

  def join_email_course(_args, _info) do
    {:error, "Authentication required"}
  end

  def decline_email_course(_args, %{context: %{spouse: spouse}}) do
    spouse
    |> Changeset.change(%{joined_course: false})
    |> Repo.update()
  end

  def decline_email_course(_args, _info) do
    {:error, "Authentication required"}
  end
end
