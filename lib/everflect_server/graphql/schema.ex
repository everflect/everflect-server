defmodule EverflectServer.GraphQL.Schema do
  @moduledoc """
  GraphQL Schema definition,
  including queries, mutations, and subscriptions
  """

  use Absinthe.Schema
  # use ApolloTracing

  alias EverflectServer.GraphQL.Resolver.{
    Account,
    Couple,
    Course,
    Inventory,
    Lesson,
    Purchase,
    PushNotification,
    Spouse
  }

  import_types(EverflectServer.GraphQL.Schema.Types)

  #### Middleware
  # def middleware(middleware, _field, _object) do
  #   [EverflectServer.GraphQL.MW.Authentication | middleware]
  # end

  #### Queries
  query do
    @desc "The currently logged-in user's information"
    field :me, :spouse do
      resolve(&Spouse.mySelf/2)
    end

    @desc "The currently logged-in user's spouse's information"
    field :spouse, :spouse do
      resolve(&Spouse.mySpouse/2)
    end

    @desc "Information about the couple the user is joined to (if any)"
    field :couple, :couple do
      resolve(&Couple.couple/2)
    end

    @desc "The join token to produce a complete couple (if available)"
    field :join, :join_token do
      resolve(&Couple.joinToken/2)
    end

    @desc "The inventory currently in use"
    field :current_inventory, :inventory do
      resolve(&Inventory.current_inventory/2)
    end

    @desc "The most recently completed inventory"
    field :previous_inventory, :inventory do
      resolve(&Inventory.previous_inventory/2)
    end

    @desc "Paginated list of inventories for the main page"
    field :inventories, list_of(:inventory) do
      arg(:limit, non_null(:integer))
      arg(:offset, non_null(:integer))

      resolve(&Inventory.paginated/2)
    end

    @desc "Specific inventory"
    field :inventory, :inventory do
      arg(:inventory_id, non_null(:string))

      resolve(&Inventory.inventory/2)
    end

    @desc "Extra information about inventories"
    field :inventory_meta, :inventory_meta do
      resolve(&Inventory.metadata/2)
    end

    @desc "All courses available"
    field :courses, list_of(:course) do
      resolve(&Course.all/2)
    end

    @desc "Lessons within a course"
    field :course_lessons, list_of(:lesson) do
      arg(:course_id, non_null(:string))

      resolve(&Lesson.course_lessons/2)
    end

    @desc "Purchases available and made"
    field :purchases, list_of(:purchase) do
      resolve(&Purchase.all/2)
    end

    @desc "A specific purchase"
    field :purchase, :purchase do
      arg(:purchase_key, non_null(:string))

      resolve(&Purchase.one/2)
    end
  end

  #### Mutations

  mutation do
    field :set_my_name, :spouse do
      arg(:name, non_null(:string))

      resolve(&Spouse.set_my_name/2)
    end

    field :setup_spouse, :spouse do
      arg(:name, non_null(:string))
      arg(:email, :string)

      resolve(&Spouse.setup_spouse/2)
    end

    field :apply_join_token, :couple do
      arg(:token, non_null(:string))

      resolve(&Couple.use_join_token/2)
    end

    field :join_email_course, :spouse do
      deprecate
      arg(:email, non_null(:string))

      resolve(&Spouse.join_email_course/2)
    end

    field :decline_email_course, :spouse do
      resolve(&Spouse.decline_email_course/2)
    end

    field :join_course, :couple do
      arg(:join_course, non_null(:boolean))

      resolve(&Couple.join_course/2)
    end

    field :start_inventory, :inventory do
      resolve(&Inventory.start_inventory/2)
    end

    field :advance_spouse_step, :inventory_advance do
      resolve(&Inventory.advance_spouse_step/2)
    end

    field :rollback_spouse_step, :inventory_advance do
      resolve(&Inventory.rollback_spouse_step/2)
    end

    field :set_spouse_goals, :inventory_advance do
      arg(:goal, non_null(:string))
      arg(:help, non_null(:string))

      resolve(&Inventory.set_spouse_goals/2)
    end

    field :set_spouse_praise, :inventory_advance do
      arg(:praise1, non_null(:string))
      arg(:praise2, non_null(:string))
      arg(:praise3, non_null(:string))

      resolve(&Inventory.set_spouse_praise/2)
    end

    field :set_spouse_improvement, :inventory_advance do
      arg(:improvement, non_null(:string))

      resolve(&Inventory.set_spouse_improvement/2)
    end

    field :schedule_everflect, :inventory_advance do
      arg(:inventory_datetime, non_null(:string))

      resolve(&Inventory.schedule_everflect/2)
    end

    field :request_login_email, :request_login_email_response do
      arg(:email, non_null(:string))

      resolve(&Account.request_login_email/2)
    end

    field :exchange_token_for_session, :exchange_token_for_session_response do
      arg(:token, non_null(:string))

      resolve(&Account.exchange_token_for_session/2)
    end

    field :logout_session, :logout_session_response do
      resolve(&Account.logout/2)
    end

    field :register_expo_push_token, :spouse, deprecate: true do
      arg(:push_token, non_null(:string))

      resolve(&PushNotification.register_push_token/2)
    end

    field :register_push_token, :spouse do
      arg(:push_token, non_null(:string))
      arg(:platform, non_null(:string))

      resolve(&PushNotification.register_push_token/2)
    end

    field :confirm_purchase, :purchase_confirmation do
      arg(:receipt, non_null(:string))
      arg(:platform, non_null(:string))
      arg(:signature, :string)

      resolve(&Purchase.confirm_purchase/2)
    end
  end

  #### Subscriptions
  # subscription do
  #   field :name_set, :spouse do
  #     topic fn args ->
  #       args.id
  #     end

  #     trigger :set_my_name, topic: fn spouse ->
  #       spouse.id
  #     end

  #     resolve fn %{name_name: spouse}, _, _ ->
  #       {:ok, spouse}
  #     end
  #   end
  # end
end
