defmodule EverflectServer.GraphQL.Schema.Types do
  use Absinthe.Schema.Notation

  @moduledoc """
  Define the types available for our schema
  """

  @desc """
  A member of a couple
  """
  object :spouse do
    field(:id, :string)
    field(:name, :string)
    field(:email, :string)
    field(:device_id, :string)
  end

  object :join_token do
    field(:token, :string)
  end

  object :couple do
    field(:id, :string)
    field(:joined_course, :boolean)
  end

  object :inventory do
    field(:id, :string)
    field(:my_step, :integer)
    field(:spouse_step, :integer)
    field(:my_goal, :string)
    field(:spouse_goal, :string)
    field(:my_goal_support, :string)
    field(:spouse_goal_support, :string)
    field(:my_praise1, :string)
    field(:my_praise2, :string)
    field(:my_praise3, :string)
    field(:spouse_praise1, :string)
    field(:spouse_praise2, :string)
    field(:spouse_praise3, :string)
    field(:my_improvement, :string)
    field(:spouse_improvement, :string)
    field(:next_inventory, :string)
    field(:inventory_status, :string)

    field(:couple, :couple)
    field(:first_spouse, :spouse)

    field(:inserted_at, :string)
    field(:updated_at, :string)

    field(:first_spouse_me, :boolean)
  end

  object :inventory_meta do
    field(:count, :integer)
  end

  object :inventory_advance do
    field(:success, :boolean)
  end

  object :course do
    field(:id, :string)
    field(:title, :string)

    field(:lessons, list_of(:lesson))

    field(:inserted_at, :string)
    field(:updated_at, :string)
  end

  object :lesson do
    field(:id, :string)
    field(:title, :string)
    field(:content, :string)
    field(:lesson_available, :boolean)

    field(:course, :course)

    field(:inserted_at, :string)
    field(:updated_at, :string)
  end

  object :purchase do
    field(:id, :string)
    field(:purchase_key, :string)
    field(:description, :string)
    field(:my_purchase, :boolean)
    field(:couple_purchase, :boolean)
  end

  object :request_login_email_response do
    field(:id, :string)
    field(:status, :string)
  end

  object :logout_session_response do
    field(:id, :string)
    field(:status, :string)
  end

  object :exchange_token_for_session_response do
    field(:id, :string)
    field(:session, :string)
  end

  object :purchase_confirmation do
    field(:id, :string)
    field(:status, :string)
  end
end
