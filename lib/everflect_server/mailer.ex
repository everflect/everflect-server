defmodule EverflectServer.Mailer do
  @moduledoc """
  Stub to hook up mailing component
  """
  use Swoosh.Mailer, otp_app: :everflect_server
end
