defmodule EverflectServer.PushNotificationManager do
  @moduledoc """
  Handles scheduling and managing various push notifications
  to our users
  """

  use GenServer

  alias EverflectServer.Repo
  alias EverflectServer.Web.FrozenTerm
  alias EverflectServer.Web.Inventory
  alias EverflectServer.Web.PushToken
  alias Pigeon.APNS
  alias Pigeon.APNS.Notification, as: AppleNotification
  alias Pigeon.FCM
  alias Pigeon.FCM.Notification, as: AndroidNotification

  require Logger

  import Ecto.Query

  # Public API
  def schedule_inventory_notification(
        %EverflectServer.Web.Inventory{} = inventory,
        datetime
      ) do
    GenServer.cast(
      __MODULE__,
      {:schedule_inventory_notification, {inventory, datetime}}
    )
  end

  def test_inventory_notification(inventory_id, seconds \\ 1) do
    notif_time =
      Timex.add(Timex.now(), %Timex.Duration{
        megaseconds: 0,
        seconds: seconds,
        microseconds: 0
      })

    query =
      from(
        i in Inventory,
        where: i.id == ^inventory_id
      )

    query
    |> Repo.one()
    |> schedule_inventory_notification(notif_time)
  end

  # GenServer Definition

  def start_link do
    GenServer.start_link(
      __MODULE__,
      # This is ignored downstream
      %{notifications: []},
      name: __MODULE__
    )
  end

  def init(_state) do
    # In 1 second
    next_check_timer_ref =
      Process.send_after(self(), :inventory_notifications, 1000)

    state =
      with frozen_state <- Repo.get(FrozenTerm, "push-notification-manager"),
           false <- is_nil(frozen_state),
           term <- frozen_state.term,
           false <- is_nil(term),
           new_state <- :erlang.binary_to_term(term) do
        Map.put(new_state, :next_check_timer_ref, next_check_timer_ref)
      else
        _ -> %{notifications: [], next_check_timer_ref: next_check_timer_ref}
      end

    {:ok, state}
  end

  def terminate(_reason, state) do
    struct =
      with old_state <- Repo.get(FrozenTerm, "push-notification-manager"),
           false <- is_nil(old_state) do
        old_state
      else
        _ ->
          %FrozenTerm{
            id: "push-notification-manager"
          }
      end

    frozen_state = :erlang.term_to_binary(state)

    struct
    |> FrozenTerm.changeset(%{term: frozen_state})
    |> Repo.insert_or_update!()
  end

  def handle_info(:inventory_notifications, %{
        notifications: notifications,
        next_check_timer_ref: next_check_timer_ref
      }) do
    Logger.info("--- Running through notifications ---")
    now = Timex.now()
    Logger.info("The time is #{now}")
    Logger.info("We have #{length(notifications)} total to review")

    notifications
    |> Enum.filter(fn {_type, _data, dt} -> Timex.diff(dt, now) <= 0 end)
    |> Enum.map(&process_notification/1)
    |> List.flatten()
    |> Enum.each(&send_push_notification/1)

    notifications_to_wait =
      notifications
      |> Enum.filter(fn {_type, _data, dt} -> Timex.diff(dt, now) > 0 end)

    Logger.info("We are deferring #{length(notifications_to_wait)}")

    {:ok, new_timer_ref, time_left_sleeping} =
      schedule_next_loop(notifications_to_wait, next_check_timer_ref)

    Logger.info("--- SLEEPING FOR #{time_left_sleeping} ---")

    {:noreply,
     %{
       notifications: notifications_to_wait,
       next_check_timer_ref: new_timer_ref
     }}
  end

  defp process_notification({type, data, _datetime}) do
    with {:ok, payload} <- notification_for_type(type, data),
         {:ok, push_tokens} <- push_tokens_for_type(type, data) do
      push_tokens
      |> Enum.map(fn {token, token_type} ->
        notification = Map.put(payload, :to, token)
        {token_type, notification}
      end)
    else
      error -> error
    end
  end

  defp push_tokens_for_type(:inventory_couple, %{my_id: spouse_id}) do
    tokens = push_tokens_for_spouse_id(spouse_id)

    {:ok, tokens}
  end

  defp push_tokens_for_type(:testing, %{user_id: user_id}) do
    tokens = push_tokens_for_spouse_id(user_id)
    {:ok, tokens}
  end

  defp push_tokens_for_spouse_id(spouse_id) do
    query =
      from(
        e in PushToken,
        where: e.spouse_id == ^spouse_id,
        select: [e.token, e.token_type]
      )

    query
    |> Repo.all()
    |> Enum.map(fn [token, token_type] ->
      token_type_atom =
        case token_type do
          "expo" -> :expo_notification
          "ios" -> :ios_notification
          "android" -> :android_notification
          _ -> :error
        end

      {token, token_type_atom}
    end)
  end

  defp send_push_notification({:expo_notification, payload}) do
    headers = [{"Content-Type", "application/json"}]
    {:ok, body} = Poison.encode(payload)

    {:ok, _response} =
      HTTPoison.post("https://exp.host/--/api/v2/push/send", body, headers)
  end

  defp send_push_notification(
         {:ios_notification,
          %{
            title: title,
            body: body,
            data: data,
            to: token,
            badge: badge,
            sound: sound
          }}
       ) do
    ""
    |> AppleNotification.new(token, "com.everflect.couples")
    |> AppleNotification.put_alert(%{
      "title" => title,
      "body" => body
    })
    |> AppleNotification.put_custom(data)
    |> AppleNotification.put_badge(badge)
    |> AppleNotification.put_sound(sound)
    |> APNS.push(on_response: &handle_apns_response/1)
  end

  defp send_push_notification(
         {:ios_notification,
          %{
            title: title,
            body: body,
            data: data,
            to: token,
            sound: sound
          }}
       ) do
    ""
    |> AppleNotification.new(token, "com.everflect.couples")
    |> AppleNotification.put_alert(%{
      "title" => title,
      "body" => body
    })
    |> AppleNotification.put_custom(data)
    |> AppleNotification.put_sound(sound)
    |> APNS.push(on_response: &handle_apns_response/1)
  end

  defp send_push_notification(
         {:android_notification,
          %{title: title, body: body, data: data, to: token}}
       ) do
    token
    |> AndroidNotification.new()
    |> AndroidNotification.put_notification(%{"title" => title, "body" => body})
    |> AndroidNotification.put_data(data)
    |> FCM.push()
  end

  defp handle_apns_response(%Pigeon.APNS.Notification{response: response}) do
    Logger.error(response)
  end

  defp notification_for_type(:inventory_couple, %{
         inventory_id: inventory_id,
         spouse_name: spouse_name
       }) do
    {:ok,
     %{
       data: %{inventory_id: inventory_id},
       title: "Let's check your progress",
       body:
         "It's been a few days since you Everflected. Check in with #{
           spouse_name
         }.",
       sound: "default",
       # ttl:
       # expiration:
       # priority:
       badge: 1
     }}
  end

  defp notification_for_type(:testing, %{title: title, body: body}) do
    {:ok,
     %{
       data: %{},
       title: title,
       body: body,
       sound: "default"
     }}
  end

  def handle_call({:length}, _from, %{
        notifications: notifications,
        next_check_timer_ref: next_check_timer_ref
      }) do
    {:reply, length(notifications),
     %{notifications: notifications, next_check_timer_ref: next_check_timer_ref}}
  end

  def handle_call({:dump}, _from, state) do
    {:reply, state, state}
  end

  def handle_cast(
        {:schedule_test_notification, {user_id, title, body, datetime}},
        %{
          notifications: notifications,
          next_check_timer_ref: next_check_timer_ref
        }
      ) do
    Logger.info("Scheduling test notification for #{datetime}")

    new_notifications =
      notifications ++
        [{:testing, %{user_id: user_id, title: title, body: body}, datetime}]

    Logger.info("Scheduling when we will wake up next")

    {:ok, new_timer_ref, time_left_sleeping} =
      schedule_next_loop(new_notifications, next_check_timer_ref)

    Logger.info("Sleeping for #{time_left_sleeping}ms")

    {:noreply,
     %{notifications: new_notifications, next_check_timer_ref: new_timer_ref}}
  end

  # Notifications are each a tuple of {:type, data, when/datetime}
  def handle_cast({:schedule_inventory_notification, {inventory, datetime}}, %{
        notifications: notifications,
        next_check_timer_ref: next_check_timer_ref
      }) do
    Logger.info(
      "Scheduling notification for inventory #{inventory.id} for #{datetime}"
    )

    inventory = Repo.preload(inventory, [:couple])
    couple = Repo.preload(inventory.couple, [:spouseA, :spouseB])

    spouse_a_notif =
      {:inventory_couple,
       %{
         my_id: couple.spouse_a_id,
         spouse_name: couple.spouseB.name,
         inventory_id: inventory.id
       }, datetime}

    spouse_b_notif =
      {:inventory_couple,
       %{
         my_id: couple.spouse_b_id,
         spouse_name: couple.spouseA.name,
         inventory_id: inventory.id
       }, datetime}

    Logger.info("Current count of notifications: #{length(notifications)}")
    new_notifications = notifications ++ [spouse_a_notif] ++ [spouse_b_notif]
    Logger.info("Updated count of notifications: #{length(new_notifications)}")

    Logger.info("Scheduling when we will wake up next")

    {:ok, new_timer_ref, time_left_sleeping} =
      schedule_next_loop(new_notifications, next_check_timer_ref)

    Logger.info("Sleeping for #{time_left_sleeping}ms")

    {:noreply,
     %{
       notifications: new_notifications,
       next_check_timer_ref: new_timer_ref
     }}
  end

  defp schedule_next_loop(notifications, timer_ref) do
    # Start the timer again in enough seconds to get the next notification
    next_notification_time =
      notifications
      |> Enum.map(fn {_type, _data, dt} ->
        Timex.diff(dt, Timex.now(), :milliseconds)
      end)
      # Default to one minute later
      |> Enum.min(fn -> 1000 * 60 end)

    time_left_sleeping = Process.read_timer(timer_ref)

    invalidate_timer_loop(next_notification_time, time_left_sleeping, timer_ref)
  end

  defp invalidate_timer_loop(next_notification_time, false, _timer_ref) do
    timer_loop(next_notification_time)
  end

  defp invalidate_timer_loop(
         next_notification_time,
         time_left_sleeping,
         timer_ref
       )
       when next_notification_time < time_left_sleeping do
    Process.cancel_timer(timer_ref)
    timer_loop(next_notification_time)
  end

  defp invalidate_timer_loop(
         _next_notification_time,
         time_left_sleeping,
         timer_ref
       ) do
    {:ok, timer_ref, time_left_sleeping}
  end

  defp timer_loop(next_wake_time) do
    ref = Process.send_after(self(), :inventory_notifications, next_wake_time)
    {:ok, ref, next_wake_time}
  end
end
