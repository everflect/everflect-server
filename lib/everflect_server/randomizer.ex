defmodule Randomizer do
  @moduledoc """
  Random string generator module.
  """

  @alpha_upcase "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  @alpha_downcase "abcdefghijklmnopqrstuvwxyz"
  @numbers "0123456789"

  @doc """
  Generate random string based on the given legth. It is also possible to generate certain type of randomise string using the options below:

  * :all - generate alphanumeric random string
  * :alpha - generate nom-numeric random string
  * :numeric - generate numeric random string
  * :upcase - generate upper case non-numeric random string
  * :downcase - generate lower case non-numeric random string

  ## Example
      iex> Iurban.String.randomizer(20) //"Je5QaLj982f0Meb0ZBSK"
  """
  def randomizer(length) do
    randomizer(length, :upcase)
  end

  def randomizer(length, :all) do
    (@alpha_upcase <> @alpha_downcase <> @numbers)
    |> do_randomizer(length)
  end

  def randomizer(length, :upcase) do
    (@alpha_upcase <> @numbers)
    |> do_randomizer(length)
  end

  @doc false
  defp get_range(length) when length > 1, do: 1..length
  defp get_range(_length), do: [1]

  @doc false
  defp do_randomizer(lists, length) do
    lists = lists |> String.split("", trim: true)

    length
    |> get_range
    |> Enum.reduce([], fn _, acc -> [Enum.random(lists) | acc] end)
    |> Enum.join("")
  end
end
