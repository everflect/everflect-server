defmodule EverflectServer.Web.RoomChannel do
  @moduledoc """
  Rough implementation of channels for chatting as PoC.
  """
  use Phoenix.Channel
  require Logger

  alias EverflectServer.Repo
  alias EverflectServer.Web.Couple
  alias EverflectServer.Web.Spouse

  import Ecto.Query, only: [where: 2, or_where: 2]

  @doc """
  Authorize socket to subscribe and broadcast events on this channel & topic

  Possible Return Values

  `{:ok, socket}` to authorize subscription for channel for requested topic

  `:ignore` to deny subscription/broadcast on this channel
  for the requested topic
  """
  # def join("rooms:lobby", message, socket) do
  #   Process.flag(:trap_exit, true)
  #   # :timer.send_interval(5000, :ping)
  #   send(self(), {:after_join, message})

  #   {:ok, socket}
  # end

  def join("welcome:connect", message, socket) do
    Process.flag(:trap_exit, true)

    device_id = message["deviceID"]

    # Find spouse by device_id
    spouse = getOrCreateSpouseByDeviceID(device_id)

    # Attach deviceID to this socket
    assigned_socket = assign(socket, :spouse, spouse)

    send(self(), {:after_join, message})

    {:ok, assigned_socket}
  end

  # def join("rooms:" <> _private_subtopic, _message, _socket) do
  #   {:error, %{reason: "unauthorized"}}
  # end

  def handle_info({:after_join, msg}, socket) do
    broadcast!(socket, "user:entered", %{user: msg["user"]})
    push(socket, "join", %{status: "connected"})
    # Push the spouse's name down if it exists
    name = socket.assigns[:spouse].name

    if not is_nil(name) do
      push(socket, "welcome:myName", %{name: name})
    end

    {:noreply, socket}
  end

  def terminate(reason, _socket) do
    Logger.info("> leave #{inspect(reason)}")
    :ok
  end

  # def handle_in("new:msg", msg, socket) do
  #   broadcast! socket, "new:msg", %{user: msg["user"], body: msg["body"]}
  #   {:reply, {:ok, %{msg: msg["body"]}}, assign(socket, :user, msg["user"])}
  # end

  def handle_in("welcome:getCouple", _body, socket) do
    # Find Couple ID based on Device ID of this socket
    spouse = socket.assigns[:spouse]

    couple =
      Couple
      |> where(spouse_a_id: ^spouse.device_id)
      |> or_where(spouse_b_id: ^spouse.device_id)
      |> Repo.one()

    :timer.sleep(1000)

    case is_nil(couple) do
      false -> push(socket, "welcome:receiveCoupleID", %{coupleID: "ABCDEFG"})
      true -> push(socket, "welcome:noCoupleID", %{})
    end

    {:reply, {:ok, %{}}, socket}
  end

  def handle_in("welcome:getJoinToken", _body, socket) do
    push(socket, "welcome:receiveJoinToken", %{joinToken: "A1B2C3D4"})
    {:reply, {:ok, %{}}, socket}
  end

  def handle_in("welcome:setName", body, socket) do
    IO.puts(body)
    {:reply, {:ok, %{}}, socket}
  end

  defp getOrCreateSpouseByDeviceID(deviceID) do
    found_spouse = Repo.get_by(Spouse, device_id: deviceID)

    spouse =
      case is_nil(found_spouse) do
        true -> createSpouseFromDeviceID(deviceID)
        false -> found_spouse
      end

    spouse
  end

  defp createSpouseFromDeviceID(deviceID) do
    changeset = Spouse.changeset(%Spouse{device_id: deviceID})
    {:ok, inserted_spouse} = Repo.insert(changeset)
    inserted_spouse
  end
end
