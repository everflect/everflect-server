defmodule EverflectServer.Web.PageController do
  use EverflectServer.Web, :controller

  alias EverflectServer.Web.LoginToken

  def index(conn, _params) do
    redirect(conn, external: "https://www.everflect.com/")
  end

  def login_redirect(conn, %{"login_token_id" => login_token_id}) do
    # Get the login token (if any) and redirect into the app
    login_token = Repo.get(LoginToken, login_token_id)

    url = url_for_token(login_token)

    if is_nil(login_token) do
      render(conn, "login_redirect_nodata.html", %{url: url})
    else
      render(conn, "login_redirect.html", %{url: url})
    end
  end

  defp url_for_token(login_token) when is_nil(login_token) == false do
    environment = Application.get_env(:everflect_server, :env)

    scheme =
      case environment do
        :prod -> "everflect-couples:/"
        _ -> "exp://192.168.1.16:19000"
      end

    "#{scheme}/+/login/token/#{login_token.token}/"
  end

  defp url_for_token(login_token) when is_nil(login_token) == true do
    environment = Application.get_env(:everflect_server, :env)

    scheme =
      case environment do
        :prod -> "everflect-couples:/"
        _ -> "exp3dfb95d82f6042c6987ab443a1f407a0:/"
      end

    "#{scheme}/+"
  end
end
