defmodule EverflectServer.Web.Couple do
  @moduledoc """
  Models a couple (aka two Spouses) and connects them to inventories
  """

  use EverflectServer.Web, :model

  alias Ecto.Changeset
  alias Ecto.Multi
  alias EverflectServer.Repo

  schema "couples" do
    belongs_to(:spouseA, EverflectServer.Web.Spouse, foreign_key: :spouse_a_id)
    belongs_to(:spouseB, EverflectServer.Web.Spouse, foreign_key: :spouse_b_id)
    field(:join_token, :string)
    field(:joined_course, :boolean)

    timestamps(type: :utc_datetime)
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:join_token, :joined_course])
    |> validate_required([])

    # |> unique_constraint([:join_token])
  end

  def join_spouse_with_token(joining_spouse, token) do
    Multi.new()
    |> Multi.update(
      :join_spouse_to_couple,
      join_spouses_changeset(String.upcase(token), joining_spouse.id)
    )
    |> Multi.delete(
      :prune_orphaned_couple,
      prune_orphaned_couple_changeset(joining_spouse.id)
    )
    |> Repo.transaction()
  end

  defp join_spouses_changeset(join_token, joining_spouse_id) do
    # Find the couple that this join_token is with
    __MODULE__
    |> Repo.get_by(join_token: join_token)
    |> Changeset.change(%{spouse_b_id: joining_spouse_id, join_token: nil})
  end

  defp prune_orphaned_couple_changeset(joining_spouse_id) do
    # Find the couple that this spouse is with
    __MODULE__
    |> Repo.get_by(spouse_a_id: joining_spouse_id)
  end

  def couple_from_spouse(spouse) do
    Repo.one(
      from(
        c in __MODULE__,
        where: [spouse_a_id: ^spouse.id],
        or_where: [spouse_b_id: ^spouse.id]
      )
    )
  end
end
