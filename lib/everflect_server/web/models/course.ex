defmodule EverflectServer.Web.Course do
  @moduledoc """
  Models an arbitrary course
  """

  use EverflectServer.Web, :model
  import Ecto.Changeset
  alias EverflectServer.Web.Course

  schema "courses" do
    field(:title, :string)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(%Course{} = course, attrs) do
    course
    |> cast(attrs, [:title])
    |> validate_required([:title])
  end
end
