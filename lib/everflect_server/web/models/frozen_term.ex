defmodule EverflectServer.Web.FrozenTerm do
  @moduledoc """
  Tracks arbitrary frozen terms
  """

  use EverflectServer.Web, :model

  @primary_key {:id, :string, []}
  schema "frozen_terms" do
    field(:term, :binary)

    timestamps()
  end

  @doc false
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:term])
    |> validate_required([:term])
    |> unique_constraint(:id, name: :frozen_terms_pkey)
  end
end
