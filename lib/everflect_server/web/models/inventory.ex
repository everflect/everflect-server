defmodule EverflectServer.Web.Inventory do
  @moduledoc """
  Models an inventory session
  """
  use EverflectServer.Web, :model

  schema "inventories" do
    field(:spouse_a_step, :integer, default: 0)
    field(:spouse_b_step, :integer, default: 0)
    field(:spouse_a_goal, EverflectServer.Encrypted.Binary)
    field(:spouse_b_goal, EverflectServer.Encrypted.Binary)
    field(:spouse_a_goal_support, EverflectServer.Encrypted.Binary)
    field(:spouse_b_goal_support, EverflectServer.Encrypted.Binary)
    field(:spouse_a_praise1, EverflectServer.Encrypted.Binary)
    field(:spouse_a_praise2, EverflectServer.Encrypted.Binary)
    field(:spouse_a_praise3, EverflectServer.Encrypted.Binary)
    field(:spouse_b_praise1, EverflectServer.Encrypted.Binary)
    field(:spouse_b_praise2, EverflectServer.Encrypted.Binary)
    field(:spouse_b_praise3, EverflectServer.Encrypted.Binary)
    field(:spouse_a_improvement, EverflectServer.Encrypted.Binary)
    field(:spouse_b_improvement, EverflectServer.Encrypted.Binary)
    field(:next_inventory, :utc_datetime)
    field(:inventory_status, :string, default: "waiting_for_both")
    belongs_to(:couple, EverflectServer.Web.Couple, foreign_key: :couple_id)

    belongs_to(
      :first_spouse,
      EverflectServer.Web.Spouse,
      foreign_key: :first_spouse_id
    )

    timestamps(type: :utc_datetime)
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [
      :spouse_a_step,
      :spouse_b_step,
      :spouse_a_goal,
      :spouse_b_goal,
      :spouse_a_goal_support,
      :spouse_b_goal_support,
      :spouse_a_praise1,
      :spouse_a_praise2,
      :spouse_a_praise3,
      :spouse_b_praise1,
      :spouse_b_praise2,
      :spouse_b_praise3,
      :spouse_a_improvement,
      :spouse_b_improvement,
      :next_inventory,
      :inventory_status,
      :couple_id,
      :first_spouse_id
    ])
    |> validate_required([
      :spouse_a_step,
      :spouse_b_step,
      :couple_id,
      :first_spouse_id,
      :inventory_status
    ])
    |> assoc_constraint(:couple)
    |> assoc_constraint(:first_spouse)
  end
end
