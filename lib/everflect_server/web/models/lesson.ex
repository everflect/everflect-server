defmodule EverflectServer.Web.Lesson do
  @moduledoc """
  Models an arbitrary lesson
  """

  use EverflectServer.Web, :model
  import Ecto.Changeset

  alias EverflectServer.Web.Course
  alias EverflectServer.Web.Lesson
  alias EverflectServer.Web.Purchase

  schema "lessons" do
    field(:content, :string)
    field(:title, :string)
    belongs_to(:course, Course, foreign_key: :course_id)
    belongs_to(:required_purchase, Purchase, foreign_key: :required_purchase_id)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(%Lesson{} = lesson, attrs) do
    lesson
    |> cast(attrs, [:title, :content, :required_purchase_id])
    |> validate_required([:title, :content])
  end
end
