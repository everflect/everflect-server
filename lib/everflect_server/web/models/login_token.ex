defmodule EverflectServer.Web.LoginToken do
  @moduledoc """
  Models a login token to authenticate a user
  """

  alias EverflectServer.Web.Spouse

  use EverflectServer.Web, :model

  schema "login_tokens" do
    field(:token, :string)
    belongs_to(:spouse, Spouse, foreign_key: :spouse_id)

    timestamps(type: :utc_datetime)
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:token, :spouse_id])
    |> validate_required([:token, :spouse_id])
    |> unique_constraint(:token)
  end
end
