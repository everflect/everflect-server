defmodule EverflectServer.Web.Purchase do
  @moduledoc """
  Represent a specific type of purchase available to users.
  Meant to be cross-referenced with spouses so that as users
  make one-time purchases, we can provide purchased function
  and content.
  """
  use EverflectServer.Web, :model
  import Ecto.Changeset
  alias EverflectServer.Web.Purchase

  schema "purchases" do
    field(:description, :string)
    field(:purchase_key, :string)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(%Purchase{} = purchase, attrs) do
    purchase
    |> cast(attrs, [:key, :description])
    |> validate_required([:purchase_key, :description])
  end
end
