defmodule EverflectServer.Web.PushToken do
  @moduledoc """
  Push notification token for a given spouse
  """

  use EverflectServer.Web, :model

  schema "push_tokens" do
    field(:token, :string)
    field(:token_type, :string)
    belongs_to(:spouse, EverflectServer.Web.Spouse, foreign_key: :spouse_id)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(push_token, attrs) do
    push_token
    |> cast(attrs, [:token, :token_type, :spouse_id])
    |> validate_required([:token, :token_type, :spouse_id])
    |> unique_constraint(:token, name: :push_tokens_spouse_id_index)
    |> unique_constraint(:token, name: :expo_push_tokens_token_spouse_id_index)
  end
end
