defmodule EverflectServer.Web.Spouse do
  @moduledoc """
  Models an individual member of a couple
  """

  alias EverflectServer.Web.Couple
  alias EverflectServer.Web.Purchase
  alias EverflectServer.Web.Spouse
  alias EverflectServer.Web.SpousePurchase

  alias EverflectServer.Repo

  use EverflectServer.Web, :model

  schema "spouses" do
    field(:name, :string)
    field(:email, :string)
    field(:device_id, Ecto.UUID)

    timestamps(type: :utc_datetime)
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    # |> validate_required([:email])
    struct
    |> cast(params, [:name, :email, :device_id])
    |> unique_constraint(:email)
  end

  def verify_purchase(spouse, purchase_key) do
    query =
      from(
        p in Purchase,
        join: sp in SpousePurchase,
        on: sp.purchase_id == p.id,
        join: s1 in Spouse,
        on: sp.spouse_id == s1.id,
        join: c in Couple,
        on: c.spouse_a_id == s1.id or c.spouse_b_id == s1.id,
        join: s2 in Spouse,
        on: c.spouse_a_id == s2.id or c.spouse_b_id == s2.id,
        distinct: true,
        where: p.purchase_key == ^purchase_key,
        where: s1.id == ^spouse.id or s2.id == ^spouse.id,
        select: {p.id}
      )

    query
    |> Repo.one()
    |> case do
      nil -> false
      _ -> true
    end
  end
end
