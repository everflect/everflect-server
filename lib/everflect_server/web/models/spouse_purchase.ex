defmodule EverflectServer.Web.SpousePurchase do
  @moduledoc """
  Model join table between Spouses and Purchases
  """

  use EverflectServer.Web, :model
  import Ecto.Changeset

  alias EverflectServer.Web.Purchase
  alias EverflectServer.Web.Spouse
  alias EverflectServer.Web.SpousePurchase

  @primary_key false
  schema "spouses_purchases" do
    belongs_to(:spouse, Spouse, foreign_key: :spouse_id)
    belongs_to(:purchase, Purchase, foreign_key: :purchase_id)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(%SpousePurchase{} = spouse_purchase, attrs) do
    spouse_purchase
    |> cast(attrs, [:spouse_id, :purchase_id])
    |> validate_required([:spouse_id, :purchase_id])
  end
end
