defmodule EverflectServer.Web.UserSession do
  @moduledoc """
  Long-running session for a given user
  """
  use EverflectServer.Web, :model

  schema "user_sessions" do
    field(:token, :string)
    belongs_to(:spouse, EverflectServer.Web.Spouse, foreign_key: :spouse_id)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(user_session, attrs \\ %{}) do
    user_session
    |> cast(attrs, [:token, :spouse_id])
    |> validate_required([:token, :spouse_id])
    |> unique_constraint(:token, name: :user_sessions_token_spouse_id_index)
  end
end
