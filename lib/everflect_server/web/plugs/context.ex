defmodule EverflectServer.Web.Plugs.Context do
  @moduledoc """
  Inject data about connection into GraphQL Context for resolvers
  """

  @behaviour Plug

  import Plug.Conn
  import Ecto.Query

  alias Ecto.UUID
  alias EverflectServer.Repo
  alias EverflectServer.Web.Couple
  alias EverflectServer.Web.Spouse
  alias EverflectServer.Web.UserSession

  def init(opts), do: opts

  def call(conn, _) do
    case build_context(conn) do
      {:ok, context} ->
        put_private(conn, :absinthe, %{context: context})

      {:error, reason} ->
        conn
        |> send_resp(403, reason)
        |> halt()

      _ ->
        conn
        |> send_resp(400, "Bad Request")
        |> halt()
    end
  end

  defp build_context(conn) do
    token_header = get_req_header(conn, "x-everflect-auth-token")

    if token_header == [] do
      build_device_context(conn)
    else
      build_token_context(conn)
    end
  end

  defp build_device_context(conn) do
    with [auth_token] <- get_req_header(conn, "x-everflect-device"),
         {:ok, casted_auth_token} <- UUID.cast(auth_token),
         {:ok, spouse} <- authorize_device(casted_auth_token),
         {:ok, couple} <- coupleFromSpouse_device(spouse) do
      # Constructed context to inject
      {:ok, %{spouse: spouse, couple: couple}}
    else
      [] -> {:ok, %{}}
      error -> error
    end
  end

  defp build_token_context(conn) do
    with [auth_token] <- get_req_header(conn, "x-everflect-auth-token"),
         {:ok, spouse_id} <- authorize(auth_token),
         {:ok, spouse} <- spouse_by_id(spouse_id),
         {:ok, couple} <- coupleFromSpouse(spouse),
         user_session <- Repo.get_by(UserSession, token: auth_token) do
      # Constructed context to inject
      {:ok, %{spouse: spouse, couple: couple, user_session_id: user_session.id}}
    else
      [] -> {:ok, %{}}
      {:error, "Invalid authorization token"} -> {:ok, %{}}
      error -> error
    end
  end

  defp spouse_by_id(spouse_id) do
    spouse =
      Spouse
      |> Repo.get(spouse_id)

    spouse
    |> is_nil
    |> case do
      true -> {:error, "No spouse matching auth token found"}
      false -> {:ok, spouse}
    end
  end

  defp coupleFromSpouse(spouse) do
    query =
      from(
        c in Couple,
        join: s in Spouse,
        on:
          (c.spouse_a_id == s.id or c.spouse_b_id == s.id) and
            s.id == ^spouse.id,
        select: {c.id}
      )

    {:ok, couple_id} =
      query
      |> Repo.one()
      |> case do
        {couple_id} -> {:ok, couple_id}
        nil -> {:ok, nil}
      end

    couple =
      Couple
      |> Repo.get(couple_id)

    {:ok, couple}
  end

  defp coupleFromSpouse_device(spouse) do
    query =
      from(
        c in Couple,
        join: s in Spouse,
        on:
          (c.spouse_a_id == s.id or c.spouse_b_id == s.id) and
            s.device_id == ^spouse.device_id,
        select: {c.id}
      )

    {:ok, couple_id} =
      query
      |> Repo.one()
      |> case do
        {couple_id} -> {:ok, couple_id}
        nil -> {:ok, nil}
      end

    couple =
      Couple
      |> Repo.get(couple_id)

    {:ok, couple}
  end

  defp authorize(auth_token) do
    auth_token
    |> getSpouseIdByAuthToken
    |> case do
      nil -> {:error, "Invalid authorization token"}
      spouse_id -> {:ok, spouse_id}
    end
  end

  defp authorize_device(auth_token) do
    auth_token
    |> getOrCreateSpouseByDeviceID
    |> case do
      nil -> {:error, "Invalid authorization token"}
      spouse -> {:ok, spouse}
    end
  end

  defp getOrCreateSpouseByDeviceID(deviceID) do
    found_spouse =
      Spouse
      |> Repo.get_by(device_id: deviceID)

    found_spouse
    |> is_nil
    |> case do
      true -> createSpouseFromDeviceID(deviceID)
      false -> found_spouse
    end
  end

  defp createSpouseFromDeviceID(deviceID) do
    transaction_result =
      Repo.transaction(fn ->
        {:ok, inserted_spouse} =
          %Spouse{device_id: deviceID}
          |> Spouse.changeset()
          |> Repo.insert()

        {:ok, inserted_couple} =
          %Couple{
            spouseA: inserted_spouse,
            join_token: Randomizer.randomizer(5)
          }
          |> Couple.changeset()
          |> Repo.insert()

        %{spouse: inserted_spouse, couple: inserted_couple}
      end)

    case transaction_result do
      {:ok, %{spouse: inserted_spouse, couple: _inserted_couple}} ->
        inserted_spouse

      {:error, :rollback} ->
        nil
    end
  end

  defp getSpouseIdByAuthToken(auth_token) do
    found_token =
      UserSession
      |> Repo.get_by(token: auth_token)

    found_token
    |> is_nil
    |> case do
      true -> nil
      false -> found_token.spouse_id
    end
  end
end
