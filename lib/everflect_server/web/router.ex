defmodule EverflectServer.Web.Router do
  use EverflectServer.Web, :router

  use Plug.ErrorHandler
  use Sentry.Plug

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  # pipeline :api do
  #   plug :accepts, ["json"]
  # end

  scope "/", EverflectServer.Web do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", PageController, :index)
    get("/loginRedirect/:login_token_id", PageController, :login_redirect)
  end

  pipeline :graphql do
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(EverflectServer.Web.Plugs.Context)
  end

  scope "/api" do
    pipe_through(:graphql)

    scope "/graphql" do
      forward("/", Absinthe.Plug, schema: EverflectServer.GraphQL.Schema)
      # ,pipeline: {ApolloTracing.Pipeline, :plug}
    end

    if Mix.env() == :dev do
      forward(
        "/graphiql",
        Absinthe.Plug.GraphiQL,
        schema: EverflectServer.GraphQL.Schema,
        socket: EverflectServer.Web.UserSocket
      )
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", EverflectServer.Web do
  #   pipe_through :api
  # end
end
