defmodule EverflectServer.Repo.Migrations.CreateSpouse do
  use Ecto.Migration

  def change do
    create table(:spouses, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:name, :string)
      add(:email, :string)
      add(:device_id, :uuid)

      timestamps()
    end

    create(unique_index(:spouses, [:device_id]))
  end
end
