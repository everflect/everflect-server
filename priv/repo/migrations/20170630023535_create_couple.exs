defmodule EverflectServer.Repo.Migrations.CreateCouple do
  use Ecto.Migration

  def change do
    create table(:couples, primary_key: false) do
      add(:id, :binary_id, primary_key: true)

      add(
        :spouse_a_id,
        references(:spouses, on_delete: :nothing, type: :binary_id)
      )

      add(
        :spouse_b_id,
        references(:spouses, on_delete: :nothing, type: :binary_id)
      )

      add(:join_token, :string)
      add(:joined_course, :boolean)

      timestamps()
    end

    create(index(:couples, [:spouse_a_id]))
    create(index(:couples, [:spouse_b_id]))
    # create unique_index(:couples, [:join_token])
  end
end
