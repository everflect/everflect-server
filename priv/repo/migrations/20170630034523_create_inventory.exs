defmodule EverflectServer.Repo.Migrations.CreateInventory do
  use Ecto.Migration

  def up do
    execute(
      "create type inventory_status as enum ('waiting_for_both', 'waiting_for_a', 'waiting_for_b', 'in_progress', 'completed')"
    )

    create table(:inventories, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:spouse_a_step, :integer, null: false, default: 0)
      add(:spouse_b_step, :integer, null: false, default: 0)
      add(:spouse_a_goal, :string, size: 1000)
      add(:spouse_b_goal, :string, size: 1000)
      add(:spouse_a_goal_support, :string, size: 1000)
      add(:spouse_b_goal_support, :string, size: 1000)
      add(:spouse_a_praise1, :string, size: 1000)
      add(:spouse_a_praise2, :string, size: 1000)
      add(:spouse_a_praise3, :string, size: 1000)
      add(:spouse_b_praise1, :string, size: 1000)
      add(:spouse_b_praise2, :string, size: 1000)
      add(:spouse_b_praise3, :string, size: 1000)
      add(:spouse_a_improvement, :string, size: 1000)
      add(:spouse_b_improvement, :string, size: 1000)
      add(:next_inventory, :utc_datetime)
      add(:inventory_status, :inventory_status, null: false)

      add(
        :couple_id,
        references(:couples, on_delete: :nothing, type: :binary_id)
      )

      add(
        :first_spouse_id,
        references(:spouses, on_delete: :nothing, type: :binary_id)
      )

      timestamps()
    end

    create(index(:inventories, [:couple_id]))
    create(index(:inventories, [:first_spouse_id]))
  end

  def down do
    drop(index(:inventories, [:couple_id]))
    drop(index(:inventories, [:first_spouse_id]))
    drop(table(:inventories))
    execute("drop type inventory_status")
  end
end
