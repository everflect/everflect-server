defmodule EverflectServer.Repo.Migrations.CreateLessons do
  use Ecto.Migration

  def change do
    create table(:lessons, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:title, :string)
      add(:content, :text)

      add(
        :course_id,
        references(:courses, on_delete: :nothing, type: :binary_id)
      )

      timestamps(type: :timestamptz)
    end

    create(index(:lessons, [:course_id]))
  end
end
