defmodule EverflectServer.Repo.Migrations.TimezoneShift do
  use Ecto.Migration

  def up do
    alter table("spouses") do
      modify(:inserted_at, :timestamptz)
      modify(:updated_at, :timestamptz)
    end

    alter table("couples") do
      modify(:inserted_at, :timestamptz)
      modify(:updated_at, :timestamptz)
    end

    alter table("inventories") do
      modify(:next_inventory, :timestamptz)
      modify(:inserted_at, :timestamptz)
      modify(:updated_at, :timestamptz)
    end
  end

  def down do
    alter table("spouses") do
      modify(:inserted_at, :timestamp)
      modify(:updated_at, :timestamp)
    end

    alter table("couples") do
      modify(:inserted_at, :timestamp)
      modify(:updated_at, :timestamp)
    end

    alter table("inventories") do
      modify(:next_inventory, :timestamp)
      modify(:inserted_at, :timestamp)
      modify(:updated_at, :timestamp)
    end
  end
end
