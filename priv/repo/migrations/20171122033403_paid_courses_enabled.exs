defmodule EverflectServer.Repo.Migrations.PaidCoursesEnabled do
  use Ecto.Migration

  def change do
    create table(:purchases, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:purchase_key, :string)
      add(:description, :string)
    end

    create table(:spouses_purchases, primary_key: false) do
      add(:spouse_id, references(:spouses, type: :binary_id), primary_key: true)

      add(
        :purchase_id,
        references(:purchases, type: :binary_id),
        primary_key: true
      )

      timestamps(type: :timestamptz)
    end

    alter table("lessons") do
      add(:required_purchase_id, :binary_id, null: true)
    end
  end
end
