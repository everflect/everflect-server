defmodule EverflectServer.Repo.Migrations.LoginTokens do
  use Ecto.Migration

  def change do
    create table(:login_tokens, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:token, :string)

      add(
        :spouse_id,
        references(:spouses, on_delete: :nothing, type: :binary_id)
      )

      timestamps(type: :timestamptz)
    end
  end
end
