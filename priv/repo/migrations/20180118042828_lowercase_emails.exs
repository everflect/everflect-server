defmodule EverflectServer.Repo.Migrations.LowercaseEmails do
  use Ecto.Migration

  def up do
    execute("CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public")

    alter table("spouses") do
      modify(:email, :citext)
    end

    create(unique_index("spouses", [:email]))
  end

  def down do
    drop(unique_index("spouses", [:email]))

    alter table("spouses") do
      modify(:email, :string)
    end

    execute("DROP EXTENSION IF EXISTS citext")
  end
end
