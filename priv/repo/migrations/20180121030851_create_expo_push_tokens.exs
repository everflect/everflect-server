defmodule EverflectServer.Repo.Migrations.CreateExpoPushTokens do
  use Ecto.Migration

  def change do
    create table(:expo_push_tokens, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:token, :string, size: 50, null: false)

      add(
        :spouse_id,
        references(:spouses, on_delete: :delete_all, type: :binary_id),
        null: false
      )

      timestamps()
    end

    create(index(:expo_push_tokens, [:spouse_id]))
    create(unique_index(:expo_push_tokens, [:token, :spouse_id]))
  end
end
