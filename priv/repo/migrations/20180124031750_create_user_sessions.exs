defmodule EverflectServer.Repo.Migrations.CreateUserSessions do
  use Ecto.Migration

  def change do
    create table(:user_sessions, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:token, :string, size: 50, null: false)

      add(
        :spouse_id,
        references(:spouses, on_delete: :delete_all, type: :binary_id),
        null: false
      )

      timestamps()
    end

    create(index(:user_sessions, [:spouse_id]))
    create(unique_index(:user_sessions, [:token, :spouse_id]))
  end
end
