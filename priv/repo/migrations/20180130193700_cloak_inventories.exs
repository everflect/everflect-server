defmodule EverflectServer.Repo.Migrations.CloakInventories do
  use Ecto.Migration

  def change do
    drop(index(:inventories, [:couple_id]))
    drop(index(:inventories, [:first_spouse_id]))
    drop(table(:inventories))

    create table(:inventories, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:spouse_a_step, :integer, null: false, default: 0)
      add(:spouse_b_step, :integer, null: false, default: 0)
      add(:spouse_a_goal, :binary)
      add(:spouse_b_goal, :binary)
      add(:spouse_a_goal_support, :binary)
      add(:spouse_b_goal_support, :binary)
      add(:spouse_a_praise1, :binary)
      add(:spouse_a_praise2, :binary)
      add(:spouse_a_praise3, :binary)
      add(:spouse_b_praise1, :binary)
      add(:spouse_b_praise2, :binary)
      add(:spouse_b_praise3, :binary)
      add(:spouse_a_improvement, :binary)
      add(:spouse_b_improvement, :binary)
      add(:next_inventory, :timestamptz)
      add(:inventory_status, :inventory_status, null: false)
      add(:encryption_version, :binary)

      add(
        :couple_id,
        references(:couples, on_delete: :nothing, type: :binary_id)
      )

      add(
        :first_spouse_id,
        references(:spouses, on_delete: :nothing, type: :binary_id)
      )

      timestamps(type: :timestamptz)
    end

    create(index(:inventories, [:couple_id]))
    create(index(:inventories, [:first_spouse_id]))
    create(index(:inventories, [:encryption_version]))
  end
end
