defmodule EverflectServer.Repo.Migrations.CreateFrozenTerms do
  use Ecto.Migration

  def change do
    create table(:frozen_terms, primary_key: false) do
      add(:id, :string, primary_key: true)
      add(:term, :binary)

      timestamps(type: :timestamptz)
    end
  end
end
