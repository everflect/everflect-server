defmodule EverflectServer.Repo.Migrations.AddPurchaseTimestamps do
  use Ecto.Migration

  def change do
    alter table(:purchases) do
      add(:inserted_at, :timestamptz)
      add(:updated_at, :timestamptz)
    end
  end
end
