defmodule EverflectServer.Repo.Migrations.AllPushNotifications do
  use Ecto.Migration

  def up do
    alter table(:expo_push_tokens) do
      add(:token_type, :string, null: false, default: "expo")
      modify(:token, :string, size: 255)
    end

    rename(table(:expo_push_tokens), to: table(:push_tokens))
  end

  def down do
    alter table(:push_tokens) do
      remove(:token_type)
      modify(:token, :string, size: 50)
    end
  end
end
