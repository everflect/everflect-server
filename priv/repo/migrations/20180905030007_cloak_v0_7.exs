defmodule EverflectServer.Repo.Migrations.CloakV07 do
  @moduledoc """
  Migration to upgrade from Cloak v0.6 to v0.7
  """
  use Ecto.Migration

  def change do
    alter table(:inventories) do
      remove(:encryption_version)
    end

    Cloak.Migrator.migrate(EverflectServer.Repo, EverflectServer.Web.Inventory)
  end
end
