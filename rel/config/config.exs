use Mix.Config

# General application configuration
config :everflect_server,
  ecto_repos: [EverflectServer.Repo],
  env: :prod

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure phoenix generators
config :phoenix, :generators, binary_id: true

config :everflect_server, EverflectServer.Mailer,
  adapter: Swoosh.Adapters.AmazonSES,
  region: System.get_env("AWS_REGION"),
  access_key: System.get_env("AWS_ACCESS_KEY"),
  secret: System.get_env("AWS_SECRET_KEY")

config :everflect_server, EverflectServer.Web.Endpoint,
  http: [port: System.get_env("PORT")],
  url: [host: System.get_env("HOST"), port: System.get_env("PORT")],
  cache_static_manifest: "priv/static/cache_manifest.json",
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  render_errors: [view: EverflectServer.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: EverflectServer.PubSub, adapter: Phoenix.PubSub.PG2],
  server: true,
  root: "."

# Do not print debug messages in production
config :logger, level: :info

# Configure your database
config :everflect_server, EverflectServer.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("DB_USERNAME"),
  password: System.get_env("DB_PASSWORD"),
  database: System.get_env("DB_NAME"),
  hostname: System.get_env("DB_HOSTNAME"),
  pool_size: 20

config :sentry,
  dsn: System.get_env("SENTRY_DSN"),
  environment_name: :prod,
  enable_source_code_context: true,
  root_source_code_path: File.cwd!(),
  tags: %{
    env: System.get_env("APP_ENV")
  },
  included_environments: [:prod]

config :everflect_server, EverflectServer.GraphQL.Resolver.Purchase,
  android_license_key: System.get_env("ANDROID_LICENSE_KEY")

config :pigeon, :apns,
  apns_default: %{
    cert: "/opt/app/certs/cert.pem",
    key: "/opt/app/certs/key_unencrypted.pem",
    mode: :prod
  }

config :pigeon, :fcm,
  fcm_default: %{
    key: System.get_env("FCM_KEY")
  }
