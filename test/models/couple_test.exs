defmodule EverflectServer.CoupleTest do
  use EverflectServer.ModelCase

  alias EverflectServer.Web.Couple

  @valid_attrs %{
    spouse_a_id: "7488a646-e31f-11e4-aace-600308960662",
    spouse_b_id: "7488a646-e31f-11e4-aace-600308960662"
  }
  # @invalid_attrs %{joined_course: nil}

  test "changeset with valid attributes" do
    changeset = Couple.changeset(%Couple{}, @valid_attrs)
    assert changeset.valid?
  end

  # test "changeset with invalid attributes" do
  #   changeset = Couple.changeset(%Couple{}, @invalid_attrs)
  #   refute changeset.valid?
  # end
end
