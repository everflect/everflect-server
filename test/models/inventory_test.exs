defmodule EverflectServer.InventoryTest do
  use EverflectServer.ModelCase

  alias EverflectServer.Web.Inventory

  @valid_attrs %{
    next_inventory: %DateTime{
      calendar: Calendar.ISO,
      day: 17,
      hour: 14,
      microsecond: {0, 6},
      minute: 0,
      month: 4,
      second: 0,
      std_offset: 0,
      time_zone: "Etc/UTC",
      utc_offset: 0,
      year: 2010,
      zone_abbr: "UTC"
    },
    spouse_a_goal: "some spouse_a_goal",
    spouse_a_goal_support: "some spouse_a_goal_support",
    spouse_a_improvement: "some spouse_a_improvement",
    spouse_a_praise1: "some spouse_a_praise1",
    spouse_a_praise2: "some spouse_a_praise2",
    spouse_a_praise3: "some spouse_a_praise3",
    spouse_b_goal: "some spouse_b_goal",
    spouse_b_goal_support: "some spouse_b_goal_support",
    spouse_b_improvement: "some spouse_b_improvement",
    spouse_b_praise1: "some spouse_b_praise1",
    spouse_b_praise2: "some spouse_b_praise2",
    spouse_b_praise3: "some spouse_b_praise3",
    couple_id: "some_couple_id",
    first_spouse_id: "some_first_spouse_id"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Inventory.changeset(%Inventory{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Inventory.changeset(%Inventory{}, @invalid_attrs)
    refute changeset.valid?
  end
end
