defmodule EverflectServer.SpouseTest do
  use EverflectServer.ModelCase

  alias EverflectServer.Web.Spouse

  @valid_attrs %{
    name: "some content",
    device_id: "7488a646-e31f-11e4-aace-600308960662"
  }
  # @invalid_attrs %{
  #   device_id: nil
  # }

  test "changeset with valid attributes" do
    changeset = Spouse.changeset(%Spouse{}, @valid_attrs)
    assert changeset.valid?
  end

  # test "changeset with invalid attributes" do
  #   changeset = Spouse.changeset(%Spouse{}, @invalid_attrs)
  #   refute changeset.valid?
  # end
end
